<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Event;

class Artist extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'photo_url' , 'facebook_url'
    ];

    public function events(){

        return $this->hasMany(Event::class);
    }
}
