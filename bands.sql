-- --------------------------------------------------------
-- Host:                         192.168.10.10
-- Server version:               5.7.24-0ubuntu0.18.04.1 - (Ubuntu)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table bands.artists
CREATE TABLE IF NOT EXISTS `artists` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `facebook_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `artists_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table bands.artists: ~5 rows (approximately)
DELETE FROM `artists`;
/*!40000 ALTER TABLE `artists` DISABLE KEYS */;
INSERT INTO `artists` (`id`, `name`, `email`, `photo_url`, `facebook_url`, `created_at`, `updated_at`) VALUES
	(21, 'Elody Mante', 'stroman.lexus@example.org', '5daff41feb9eced5fda24420dfa2d45d.jpg', 'http://www.auer.info/earum-illum-eaque-ut-temporibus-porro-enim-quas', '2019-10-03 07:09:28', '2019-10-03 07:09:28'),
	(22, 'Kadin Walsh', 'clementina.windler@example.com', '1cb9d225db71efdf0da17299693d655e.jpg', 'https://fahey.com/a-libero-occaecati-excepturi-veritatis-est-provident.html', '2019-10-03 07:09:28', '2019-10-03 07:09:28'),
	(23, 'Ariane Rolfson', 'nikolaus.helen@example.com', 'e4fef209f10f398c1e8447d8d5709bea.jpg', 'http://huel.com/', '2019-10-03 07:09:28', '2019-10-03 07:09:28'),
	(24, 'Dejuan Renner', 'ukilback@example.net', '71cfa386fdda77b72f8a43c348280f1a.jpg', 'http://www.stokes.org/molestiae-ex-eaque-sed-non-sit-et-impedit', '2019-10-03 07:09:28', '2019-10-03 07:09:28'),
	(25, 'Mr. Anderson Mills III', 'kay75@example.org', 'ce046e8976c1391fd0760efe793629c1.jpg', 'http://hagenes.biz/repellendus-sapiente-inventore-nam-eligendi', '2019-10-03 07:09:28', '2019-10-03 07:09:28');
/*!40000 ALTER TABLE `artists` ENABLE KEYS */;

-- Dumping structure for table bands.events
CREATE TABLE IF NOT EXISTS `events` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `artist_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `venue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table bands.events: ~25 rows (approximately)
DELETE FROM `events`;
/*!40000 ALTER TABLE `events` DISABLE KEYS */;
INSERT INTO `events` (`id`, `artist_id`, `country`, `city`, `venue`, `date`, `created_at`, `updated_at`) VALUES
	(56, '21', 'Niue', 'Dockmouth', '346 Devin Mill Apt. 880\nNorth Paulineburgh, OR 35962-7080', '2019-10-03', '2019-10-03 07:09:59', '2019-10-03 07:09:59'),
	(57, '21', 'Liberia', 'New Emersonchester', '502 Swaniawski Canyon Suite 939\nDereckchester, TN 03425-6739', '2019-10-03', '2019-10-03 07:09:59', '2019-10-03 07:09:59'),
	(58, '21', 'Botswana', 'Dickibury', '844 Demario Roads Apt. 408\nHandmouth, TX 42738', '2019-10-03', '2019-10-03 07:09:59', '2019-10-03 07:09:59'),
	(59, '21', 'Tokelau', 'Angelinaville', '1046 Harvey Gardens Apt. 662\nBartonshire, NY 25537-6833', '2019-10-03', '2019-10-03 07:09:59', '2019-10-03 07:09:59'),
	(60, '21', 'Brazil', 'East Ellenport', '6637 Kihn Village\nSouth Erick, VA 64460', '2019-10-03', '2019-10-03 07:09:59', '2019-10-03 07:09:59'),
	(61, '22', 'Costa Rica', 'Orlomouth', '1913 Rebekah Passage Suite 844\nLake Florian, DC 34769', '2019-10-03', '2019-10-03 07:09:59', '2019-10-03 07:09:59'),
	(62, '22', 'Indonesia', 'Geovanyshire', '8470 Treutel Stravenue Apt. 686\nEast Cleorachester, RI 01035', '2019-10-03', '2019-10-03 07:09:59', '2019-10-03 07:09:59'),
	(63, '22', 'Gambia', 'Burdettehaven', '97223 Goyette Ridge\nLolamouth, NY 19109-1798', '2019-10-03', '2019-10-03 07:09:59', '2019-10-03 07:09:59'),
	(64, '22', 'Ecuador', 'North Merlstad', '1831 Thelma Crossing Suite 759\nWillmsborough, TN 93951', '2019-10-03', '2019-10-03 07:09:59', '2019-10-03 07:09:59'),
	(65, '22', 'Armenia', 'Loweside', '1264 Davis Lodge\nBergnaumborough, CO 47515', '2019-10-03', '2019-10-03 07:09:59', '2019-10-03 07:09:59'),
	(66, '23', 'Japan', 'Devantechester', '43111 McKenzie Valley\nMarilouhaven, ME 03519-1169', '2019-10-03', '2019-10-03 07:09:59', '2019-10-03 07:09:59'),
	(67, '23', 'Namibia', 'North Armani', '43264 Davis Crossroad\nNorth Savion, TN 89168', '2019-10-03', '2019-10-03 07:09:59', '2019-10-03 07:09:59'),
	(68, '23', 'Namibia', 'Lake Arnulfobury', '61924 Pattie Bridge\nSouth Irwin, NC 07958', '2019-10-03', '2019-10-03 07:09:59', '2019-10-03 07:09:59'),
	(69, '23', 'Sri Lanka', 'Port Jessycabury', '37391 Morar Trafficway Apt. 876\nNorth Teagan, KY 48298', '2019-10-03', '2019-10-03 07:09:59', '2019-10-03 07:09:59'),
	(70, '23', 'China', 'East Aydenhaven', '7043 Gottlieb Flats\nMayrastad, NM 66182-6274', '2019-10-03', '2019-10-03 07:09:59', '2019-10-03 07:09:59'),
	(71, '24', 'Cameroon', 'Torpville', '61742 Jean Dam Suite 766\nHomenickland, ME 85937-7113', '2019-10-03', '2019-10-03 07:09:59', '2019-10-03 07:09:59'),
	(72, '24', 'Georgia', 'Lake Williamfurt', '146 Auer Coves Suite 322\nBraunside, CA 91706-9477', '2019-10-03', '2019-10-03 07:09:59', '2019-10-03 07:09:59'),
	(73, '24', 'Suriname', 'South Kassandra', '16671 Kyleigh Expressway Apt. 369\nStiedemannmouth, TX 05873', '2019-10-03', '2019-10-03 07:09:59', '2019-10-03 07:09:59'),
	(74, '24', 'Georgia', 'South Pedro', '293 Colby Valleys Apt. 720\nMortonport, TN 05500-9528', '2019-10-03', '2019-10-03 07:09:59', '2019-10-03 07:09:59'),
	(75, '24', 'Faroe Islands', 'Edwinaburgh', '2727 Imogene Village Apt. 655\nWest Ramiroview, MO 98938-6967', '2019-10-03', '2019-10-03 07:09:59', '2019-10-03 07:09:59'),
	(76, '25', 'Qatar', 'Rathburgh', '4279 Shyann Coves Apt. 067\nCraigland, FL 84939-4372', '2019-10-03', '2019-10-03 07:09:59', '2019-10-03 07:09:59'),
	(77, '25', 'Swaziland', 'Torpfort', '94477 Tressa Landing Apt. 460\nSouth Koby, VA 39243', '2019-10-03', '2019-10-03 07:09:59', '2019-10-03 07:09:59'),
	(78, '25', 'Cambodia', 'East Victoriashire', '8443 Heller Estate\nGlenbury, WY 67520', '2019-10-03', '2019-10-03 07:09:59', '2019-10-03 07:09:59'),
	(79, '25', 'Switzerland', 'Allisonmouth', '409 Mosciski Ports Suite 554\nSchuppemouth, CT 64596', '2019-10-03', '2019-10-03 07:09:59', '2019-10-03 07:09:59'),
	(80, '25', 'Mongolia', 'East Kaelaberg', '2645 Pedro Well Suite 068\nNew Roberta, NC 50821-9616', '2019-10-03', '2019-10-03 07:09:59', '2019-10-03 07:09:59');
/*!40000 ALTER TABLE `events` ENABLE KEYS */;

-- Dumping structure for table bands.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table bands.failed_jobs: ~0 rows (approximately)
DELETE FROM `failed_jobs`;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Dumping structure for table bands.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table bands.migrations: ~7 rows (approximately)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2019_08_19_000000_create_failed_jobs_table', 1),
	(4, '2019_10_02_160648_add_photo_url_to_users_table', 2),
	(5, '2019_10_02_171335_create_events_table', 3),
	(6, '2019_10_03_061053_create_artists_table', 4),
	(7, '2019_10_03_062744_drop_user_id_from_events_table', 5),
	(8, '2019_10_03_062842_add_artist_id_to_events_table', 6);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table bands.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table bands.password_resets: ~0 rows (approximately)
DELETE FROM `password_resets`;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Dumping structure for table bands.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `photo_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table bands.users: ~5 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `photo_url`) VALUES
	(16, 'River Heidenreich', 'nwalsh@example.net', '2019-10-03 06:38:28', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '3pDxVvVRd6', '2019-10-03 06:38:29', '2019-10-03 06:38:29', 'baa8ed65bdc553d812cdd8a809ceb14c.jpg'),
	(17, 'Dr. Cielo Gleichner PhD', 'xlangworth@example.net', '2019-10-03 06:38:29', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'GBHLTUysKo', '2019-10-03 06:38:29', '2019-10-03 06:38:29', '44a684e8c4148721c3c6118a3548d7a3.jpg'),
	(18, 'Miss Syble Blick DDS', 'halvorson.krystina@example.net', '2019-10-03 06:38:29', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '3FutQwYpBU', '2019-10-03 06:38:30', '2019-10-03 06:38:30', '3e653c8cf98340ed98291a0966b50016.jpg'),
	(19, 'Norris Jacobson', 'amanda38@example.org', '2019-10-03 06:38:30', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'jq73JTvXl1', '2019-10-03 06:38:31', '2019-10-03 06:38:31', '3c918238a81f67d6584909f0df750476.jpg'),
	(20, 'Mr. Daryl Howe I', 'russ.schuppe@example.org', '2019-10-03 06:38:31', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '2S6H6CNu75', '2019-10-03 06:38:31', '2019-10-03 06:38:31', '3152de07f384b44be44acb9fb0f85877.jpg');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
