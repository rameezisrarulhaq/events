<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Event;
use Faker\Generator as Faker;

$factory->define(Event::class, function (Faker $faker) {
    return [
        'artist_id' => function(){
            return factory('App\Artist')->create()->id;
        },
        'country' => $faker->country,
        'city' => $faker->city,
        'venue' => $faker->address,
        'date' => now(),  
    ];
});
