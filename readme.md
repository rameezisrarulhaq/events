
### Note
Note: I haven't received the APP ID from the https://app.swaggerhub.com/apis/Bandsintown/PublicAPI/3.0.0. I have emailed them couple of times but in vain. After waiting for days, I created a small Database Scheme for the task.


### Technology Stack:

    1) Laravel
    2) Vue
    3) Tailwindcss


### How to make it run
In order to make it run asap, do the following steps
    1) Connect the database provided
    2) Run on wamp or xamp (https://medium.com/@essienekanem/how-to-create-you-first-laravel-app-with-wamp-server-and-composer-2bc471274c16)
    3) Install Composer dependencies i.e composer install
    4) Install node dependencies ie. npm install
    

Features: 
    1) Search the artist name(as filled in the database i.e Kadin) and hit Search button or press enter or a character 'r' and search
    2) Click on the 'Show Events', it will list all the events of the artist.
    3) If you click without typing anything on the searchbox, it will return all the artist.
    4) For going back and erasing text, click on the 'Clear' cta.

    

