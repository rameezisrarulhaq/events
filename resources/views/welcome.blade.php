<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <link rel="stylesheet" href="{{ mix('/css/app.css') }}">

    </head>
    <body>
        <div class="flex-center position-ref full-height">
            
           


            <div class="container mx-auto py-12" id="app">
            
                <h2>Artist & Events</h2>

               <search></search>

            </div>
            
        </div>

    <script src="{{ mix('/js/app.js')}}"></script>

        <div class="container">

            <p class="footerP">Powered by <a href="https://www.linkedin.com/in/rameez-israr"><i>RameezIsrar</i></a></p>

        </div>
    </body>
</html>
